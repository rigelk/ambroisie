import { server } from 'node_erlastic'
import webtorrent from 'webtorrent-hybrid'
import { pick, throttle } from 'lodash'
const cfg = require('application-config')('Ambroisie/WebTorrent')

interface Torrent {
  name: string
  magnetURI: string
  path: string
}

// CLIENT CONFIG
let saved = {
  torrents: []
} as { torrents: Torrent[] }
const opts = {
  maxConns: 55, // default value = 55, value per torrent
  tracker: true, // default = true
  dht: {
    //bootstrap: [],   // bootstrap servers (default: router.bittorrent.com:6881, router.utorrent.com:6881, dht.transmissionbt.com:6881)
    host: false,     // host of local peer, if specified then announces get added to local table (String, disabled by default)
    concurrency: 16, // k-rpc option to specify maximum concurrent UDP requests allowed (Number, 16 by default)
    //hash: Function,  // custom hash function to use (Function, SHA1 by default),
    //krpc: krpc(),     // optional k-rpc instance
    timeBucketOutdated: 900000, // check buckets every 15min
  }, // default = true
  webSeeds: true // default = true
}
const client = new webtorrent(opts)
client.on('error', err => {
  if (typeof(err) != 'string') err = JSON.stringify(err); 
  process.stderr.write(err)
})
client.on('torrent', function saveTorrents () {
  saved.torrents = client.torrents.map(torrent => {
    return {
      name: torrent.name,
      magnetURI: torrent.magnetURI,
      path: torrent.path
    } as Torrent
  })

  cfg.write({
    torrents: saved.torrents
  })
})

// CLIENT HELPERS
function restoreSession (data?) {
  if (data.restore && data.state) {
    saved = data.state
  } else if (data.restore) {
    if (data.path) cfg.filePath = data.path

    cfg.read(function (err, data) {
      if (err) console.error(err)
      saved = data
    })
  }
  if (!saved.torrents) saved.torrents = []
  saved.torrents.forEach(torrent => {
    client.add(torrent.magnetURI, Object.assign({},{
      path: torrent.path
    }))
  })
}

// RETURN HELPERS
const reduce = torrents => torrents.map(t => reduceT(t))
const reduceT = torrent => {
  const keys = (torrent.progress == 1) ? [
    'infoHash',
    'magnetURI',
    'torrentFile',
    'progress',
    'timeRemaining',
    'received',
    'downloaded',
    'uploaded',
    'downloadSpeed',
    'uploadSpeed',
    'progress',
    'numPeers',
    'path',
    ] : [
      'infoHash',
      'magnetURI',
      'torrentFile',
      'progress'
    ] 
  return pick(torrent, keys)
}
const reduceC = client => {
  const keys = [
      'ratio',
      'progress',
      'uploadSpeed',
      'downloadSpeed'
    ] 
  return pick(client, keys)
}
const c = throttle((torrents, status = 'running') => { 
  return { status, torrents }
}, 500)
const t = (torrent, status = 'seeding') => {
  return { status: status, ...torrent }
}

server(
  (term: any, from: any, state: Torrent[], done: any) => {
    if (term == 'getAll') {
      return done('reply', c(reduce(client.torrents)))
    }

    if (term[0] == 'getSingle') {
      let torrent = client.get(term[1])
      if (!torrent) return done('reply', c([]))
      return done('reply', c([t(reduceT(torrent), 'unknown')]))
    }

    if (term == 'getClientOptions') {
      return done('reply', opts)
    }

    if (term == 'getClientStats') {
      return done('reply', reduceC(client))
    }

    if (term == 'getClientState') {
      return done('reply', saved)
    }

    if (term[0] == 'add') {
      let args = (term[1] instanceof Array) ? term[1] : [term[1]]
      client.add(...args, torrent => {
        torrent.on('ready', () => from(c([t(reduceT(torrent), 'added')])))
        torrent.on('download', (bytes) => from(c([t(reduceT(torrent), 'download')])))
        torrent.on('upload', (bytes) => from(c([t(reduceT(torrent), 'upload')])))
        torrent.on('done', () => from(c([t(reduceT(torrent), 'done')])))
      })
      return done('noreply')
    }

    if (term[0] == 'remove') {
      client.remove(term[1])
      return done('noreply')
    }

    if (term[0] == 'resume') {
      client.seed(term[1])
      return done('noreply')
    }
    
    if (term[0] == 'seed') {
      let args = (term[1] instanceof Array) ? term[1] : [term[1]]
      client.seed(...args, torrent => {
        from(c([t(reduceT(torrent), 'seeding')]))
      })
      return done('noreply')
    }

    if (term[0] == 'destroy') {
      client.destroy(() => {
        from('client destroyed successfully')
        process.exit()
      })
      return done('noreply')
    }

    throw new Error('unexpected request')
  },
  (term) => { // only called at process init
    restoreSession(term)
  }
)