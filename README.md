# Ambroisie

An Elixir library wrapping a webtorrent-hybrid process as an OTP application.

## Installation

You can add `ambroisie` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ambroisie, git: "git@framagit.org:rigelk/ambroisie.git", branch: "master"}
  ]
end
```

## Usage

It's straightforward to get going:

```elixir
iex(1)> Ambroisie.add 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'
:ok
iex(2)> Ambroisie.get 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'
%{
  status: "running",
  torrents: [
    %{
      downloadSpeed: 0,
      downloaded: 129241752,
      infoHash: "6a9759bffd5c0af65319979fb7832189f4f3c35d",
      magnetURI: "magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d&dn=sintel.mp4&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com",
      numPeers: 2,
      path: "/tmp/webtorrent/6a9759bffd5c0af65319979fb7832189f4f3c35d",
      progress: 1,
      received: 0,
      status: "unknown",
      timeRemaining: 0,
      torrentFile: <<100, 56, 58, 97, 110, 110, 111, 117, 110, 99, 101, 50, 54,
        58, 119, 115, 115, 58, 47, 47, 116, 114, 97, 99, 107, 101, 114, 46, 98,
        116, 111, 114, 114, 101, 110, 116, ...>>,
      uploadSpeed: 0,
      uploaded: 0
    }
  ]
}
iex(3)> Ambroisie.remove 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'
:ok
iex(4)> Ambroisie.seed '/tmp/videos/sintel.mp4'
:ok
```

For further examples, as well as the rest of the API, please generate the documentation via `mix docs`.

## Persistence

The WebTorrent subprocess can and will store its state on disk. At startup of an Ambroisie process, you get to choose wether to load this state, start anew or load a state of your own!

```elixir
iex(1)> AmbroisieApp.start(Ambroisie, %{restore: true})
:ok
```

You can inject a state:

```elixir
iex(1)> AmbroisieApp.start(Ambroisie, %{restore: true, state: %{torrents: [
  %{
    magnetURI: 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d',
    path: '/tmp/webtorrent/sintel' #optional
  }
]}})
:ok
```

You can also give a path to the [application-config](https://www.npmjs.com/package/application-config) that the subprocess loads and stores to (see the package for its default location - but probably something like `~/.config/Ambroisie/WebTorrent/config.json`):

```elixir
iex(1)> AmbroisieApp.start(Ambroisie, %{restore: true, path: '/tmp/state.json'})
:ok
```

You can query Ambroisie for the state of its client via:

```elixir
iex(1)> Ambroisie.getClientState
%{torrents: []}
```