defmodule Mix.Tasks.Compile.Typescript do
  use Mix.Task

  def run(_args) do
    cmd("npm", ["install", "--loglevel=error"], "#{:code.priv_dir(:ambroisie)}/", %{}, true)

    case cmd("tsc", [], "#{:code.priv_dir(:ambroisie)}/", %{}, true) do
      0 ->
        :ok

      exit_status ->
        raise_build_error("tsc", exit_status, "Typescript failed.")
    end
  end

  defp cmd(exec, args, cwd, env, verbose?) do
    opts = [
      into: IO.stream(:stdio, :line),
      stderr_to_stdout: true,
      cd: cwd,
      env: env
    ]

    if verbose? do
      print_verbose_info(exec, args)
    end

    {%IO.Stream{}, status} = System.cmd(find_executable(exec), args, opts)
    status
  end

  defp find_executable(exec) do
    System.find_executable(exec) ||
      Mix.raise("""
      "#{exec}" not found in the path. If you have set the MAKE environment variable,
      please make sure it is correct.
      """)
  end

  defp raise_build_error(exec, exit_status, error_msg) do
    Mix.raise(~s{Could not compile with "#{exec}" (exit status: #{exit_status}).\n} <> error_msg)
  end

  defp print_verbose_info(exec, args) do
    args =
      Enum.map_join(args, " ", fn arg ->
        if String.contains?(arg, " "), do: inspect(arg), else: arg
      end)

    Mix.shell().info("Compiling WebTorrent client (Javascript): #{exec} #{args}")
  end
end
