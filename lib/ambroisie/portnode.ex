defmodule Ambroisie.Proc do
  @moduledoc false

  @doc """
  A GenServer which starts a port, adapted from Exos.Proc to
  use GenStage instead of GenEvent.
  """
  use GenServer
  alias :erlang, as: Erl

  @doc """
  Launch a GenServer which starts a port and proxify cast and call to
  it using a port protocol with `packet: 4`, (32bits-length+data)
  messages are transmitted throught stdin/out. Input terms are
  encoded using `binary_to_term` and received terms are decoded using
  `term_to_binary`.

  - `cmd` is the shell command to launch the port
  - when the port starts, it automatically receives as first message the `init`
    term if `init !== :no_init`
  - `opts` are options for `Port.open` (for instance `[cd: "/path/"]`)
  - `link_opts` are options for `GenServer.start_link` (for instance `[name: :servername]`)
  - messages received from the port outside of a `GenServer.call`
    context are sent to the `event_manager` GenEvent if it is not `nil`
  - to allow easy supervision, if the port die with a return code == 0, then
    the GenServer die with the reason `:normal`, else with the reason `:port_terminated`
  """
  def start_link(cmd, init, opts \\ [], link_opts \\ [], event_manager \\ nil) do
    GenServer.start_link(Ambroisie.Proc, {cmd, init, opts, event_manager}, link_opts)
    QueueBroadcaster.start_link()
  end

  def init({cmd, initarg, opts}), do: init({cmd, initarg, opts, nil})

  def init({cmd, initarg, opts, event_manager}) do
    port = Port.open({:spawn, '#{cmd}'}, [:binary, :exit_status, packet: 4] ++ opts)
    if initarg !== :no_init, do: send(port, {self(), {:command, Erl.term_to_binary(initarg)}})
    {:ok, {port, event_manager}}
  end

  def handle_info({port, {:exit_status, 0}}, {port, _} = state), do: {:stop, :normal, state}

  def handle_info({port, {:exit_status, _}}, {port, _} = state),
    do: {:stop, :port_terminated, state}

  def handle_info({port, {:data, b}}, {port, event_manager} = state) do
    if event_manager do
      QueueBroadcaster.sync_notify(Erl.binary_to_term(b))
    end

    {:noreply, state}
  end

  def handle_cast(term, {port, _} = state) do
    send(port, {self(), {:command, Erl.term_to_binary(term)}})
    {:noreply, state}
  end

  def handle_call(term, _reply_to, {port, _} = state) do
    send(port, {self(), {:command, Erl.term_to_binary(term)}})

    res =
      receive do
        {^port, {:data, b}} ->
          Erl.binary_to_term(b)

        # catch exit msg and resend it
        {^port, {:exit_status, _}} = exit_msg ->
          send(self(), exit_msg)
          {:error, :port_terminated}
      end

    {:reply, res, state}
  end
end

defmodule QueueBroadcaster do
  @moduledoc false
  use GenStage

  @doc "Starts the broadcaster."
  def start_link() do
    GenStage.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @doc "Sends an event and returns only after the event is dispatched."
  def sync_notify(event, timeout \\ 5000) do
    GenStage.call(__MODULE__, {:notify, event}, timeout)
  end

  ## Callbacks

  def init(:ok) do
    {:producer, {:queue.new(), 0}, dispatcher: GenStage.BroadcastDispatcher}
  end

  def handle_call({:notify, event}, from, {queue, pending_demand}) do
    queue = :queue.in({from, event}, queue)
    dispatch_events(queue, pending_demand, [])
  end

  def handle_demand(incoming_demand, {queue, pending_demand}) do
    dispatch_events(queue, incoming_demand + pending_demand, [])
  end

  defp dispatch_events(queue, 0, events) do
    {:noreply, Enum.reverse(events), {queue, 0}}
  end

  defp dispatch_events(queue, demand, events) do
    case :queue.out(queue) do
      {{:value, {from, event}}, queue} ->
        GenStage.reply(from, :ok)
        dispatch_events(queue, demand - 1, [event | events])

      {:empty, queue} ->
        {:noreply, Enum.reverse(events), {queue, demand}}
    end
  end
end

defmodule WebTorrentConsumer do
  # When you call use in your module, the __using__ macro is called.
  defmacro __using__(_params) do
    quote do
      # User modules must implement the Filter callbacks
      @behaviour GenStage

      @doc "Starts the consumer."
      def start_link() do
        GenStage.start_link(__MODULE__, :ok)
      end

      def init(:ok) do
        # Starts a permanent subscription to the broadcaster
        # which will automatically start requesting items.
        {:consumer, :ok, subscribe_to: [QueueBroadcaster]}
      end

      def handle_events(events, _from, state) do
        for event <- events do
          IO.inspect({self(), event})
        end

        {:noreply, [], state}
      end

      # Defoverridable makes the given functions in the current module overridable
      # Without defoverridable, new definitions of greet will not be picked up
      defoverridable handle_events: 3
    end
  end
end
