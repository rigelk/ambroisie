defmodule Ambroisie do
  @moduledoc """
  Wrapper for the WebTorrent library.
  """
  use WebTorrentConsumer

  @node_process "node --no-warnings runtime.js"

  @doc false
  def start_link(initial_state) do
    Ambroisie.Proc.start_link(
      @node_process,
      initial_state,
      [cd: "#{:code.priv_dir(:ambroisie)}/dist"],
      [name: WebTorrent],
      __MODULE__
    )

    GenStage.start_link(__MODULE__, :ok)
  end

  @doc false
  def init(_) do
    Process.flag(:trap_exit, true)
    {:ok, {}}
  end

  @doc false
  def terminate(_reason, _state) do
    destroy()
    {:ok}
  end

  ## Client API
  @typedoc """
  Just a number followed by a string.
  """
  @type torrent_id :: charlist | :binary

  @doc """
  Get a list of all torrents the client manages.
  """
  def get(), do: GenServer.call(WebTorrent, :getAll, :infinity)
  def getClientState(), do: GenServer.call(WebTorrent, :getClientState, :infinity)
  def getClientStats(), do: GenServer.call(WebTorrent, :getClientStats, :infinity)
  def getClientOptions(), do: GenServer.call(WebTorrent, :getClientOptions, :infinity)

  @doc """
  Get a specific entry of the list of torrents managed by the client.
  """
  def get(torrentId), do: GenServer.call(WebTorrent, {:getSingle, torrentId})

  @doc """
  Orders the WebTorrent client to download the torrent corresponding to `torrentId`

  ## Parameters

    - torrentId: can be one of:

      - magnet uri (charlist)
      - torrent file (~~~buffer or~~~ binary)
      - info hash (charlist of hex)
      - ~~~parsed torrent (from [parse-torrent](https://github.com/webtorrent/parse-torrent))~~~
      - http/https url to a torrent file (charlist)
      - filesystem path to a torrent file (charlist)

  Returns `:ok`.

  ## Examples

      iex> Ambroisie.add 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'
      :ok

  """
  def add(torrentId), do: GenServer.cast(WebTorrent, {:add, torrentId})

  @doc """
  Orders the WebTorrent client to download the torrent corresponding to `torrentId`,
  with a Map of parameters (destination, maximum number of simultaneous connections,
  list of trackers to announce overriding WebTorrent's defaults)

  ## Parameters

    - torrentId: can be one of:

      - magnet uri (charlist)
      - torrent file (~~~buffer or~~~ binary)
      - info hash (charlist of hex)
      - ~~~parsed torrent (from [parse-torrent](https://github.com/webtorrent/parse-torrent))~~~
      - http/https url to a torrent file (charlist)
      - filesystem path to a torrent file (charlist)

    - opts: can override the following options:

  ```js
  %{
    announce: [charlist],      // Torrent trackers to use (added to list in .torrent or magnet uri)
    maxWebConns: Number,       // Max number of simultaneous connections per web seed [default=4]
    path: charlist,            // Folder to download files to (default=`/tmp/webtorrent/`)
  }
  ```

  Returns `:ok`.

  ## Examples

      iex> Ambroisie.add 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d' %{
      ...>   path: '/tmp/webtorrent/sintel'
      ...> }
      :ok

  """
  def add(torrentId, opts), do: GenServer.cast(WebTorrent, {:add, [torrentId, opts]})

  @doc """
  Orders the WebTorrent client to create a torrent for the file at
  `path` and seed it, with an optional Map of parameters `opts`.

  ## Parameters

    - opts: should be a Map holding any and possibly all of:

      - name of the torrent, under key :name

  Returns `:ok`.

  ## Examples

      iex> Ambroisie.seed '/tmp/webtorrent/sintel/sintel_trailer.mp4' %opts{
      ...>   name: 'Sintel Trailer'
      ...> }
      :ok

  """
  def seed(path, opts), do: GenServer.cast(WebTorrent, {:seed, [path, opts]})
  def seed(path), do: seed(path, [])

  @doc """
  Orders the WebTorrent client to remove the `torrentId` instance,
  without actually deleting the data (note this is normal behaviour
  for WebTorrent, and you should implement the cleaning yourself).

  Returns `:ok`.

  ## Examples

      iex> Ambroisie.remove 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'
      :ok

  """
  def remove(torrentId), do: GenServer.cast(WebTorrent, {:remove, torrentId})
  defp destroy, do: GenServer.call(WebTorrent, :destroy, :infinity)

  ## Server API

  @doc false
  def handle_events(events, _from, state) do
    statusOf = fn %{torrents: [x]} -> x[:status] end
    idOf = fn %{torrents: [x]} -> x[:infoHash] end

    for event <- events do
      IO.inspect({self(), event})

      unless is_binary(event) do
        statusOf.(event)
        |> case do
          status
          when status in [
                 :done,
                 :seeding,
                 :upload
               ] ->
            Stash.set(:webtorrent_store, idOf.(event), List.first(events[:torrents]))

          _ ->
            nil
        end
      end
    end

    {:noreply, [], state}
  end
end

defmodule AmbroisieApp do
  use Application

  def start(_type, args) do
    import Supervisor.Spec, warn: false

    ambroisie_opts = args

    supervisor_opts = [
      strategy: :one_for_one,
      name: __MODULE__,
      max_restarts: 3,
      max_seconds: 5
    ]

    children = [
      worker(Ambroisie, [ambroisie_opts])
    ]

    Supervisor.start_link(children, supervisor_opts)
  end
end
