defmodule AmbroisieBasicTest do
  use ExUnit.Case, async: false
  import TimeHelper

  setup_all do
    AmbroisieApp.start(Ambroisie, [])
    :ok
  end

  @sintel_trailer 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'

  describe "should do basic stuff like" do
    test "getting the list of torrents running" do
      list = Ambroisie.get()[:torrents]
      assert [] = list
      assert 0 = length(list)
    end

    test "adding a torrent" do
      assert Ambroisie.add(@sintel_trailer) == :ok
      wait_until(fn -> assert 1 = length(Ambroisie.get(@sintel_trailer)[:torrents]) end)
    end

    test "deleting a torrent" do
      Ambroisie.remove(@sintel_trailer)
      wait_until(fn -> assert 0 = length(Ambroisie.get(@sintel_trailer)[:torrents]) end)
    end
  end

  describe "should create torrents" do
    test "with just the path" do
      # Ambroisie.seed()
    end
  end
end

defmodule AmbroisieStateTest do
  use ExUnit.Case, async: false
  import TimeHelper

  @sintel_trailer %{
    name: "Sintel Trailer",
    magnetURI: "magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d",
    path: "/tmp/webtorrent"
  }

  describe "should be able to restore state" do
    setup do
      on_exit fn -> AmbroisieApp.stop([]) end
      :ok
    end

    test "(restore disabled)" do
      AmbroisieApp.start(Ambroisie, %{restore: false})
      assert 0 = length(Ambroisie.get()[:torrents])
    end

    test "(restore with a given state)" do
      AmbroisieApp.start(Ambroisie, %{restore: true, state: %{torrents: [@sintel_trailer]}})
      wait_until(fn -> assert 1 = length(Ambroisie.get()[:torrents]) end)
    end

    test "(restore with the previous state)" do
      AmbroisieApp.start(Ambroisie, %{restore: true})
      wait_until(fn ->
        assert length(Ambroisie.getClientState()[:torrents]) > 0
        assert List.first(Ambroisie.getClientState()[:torrents])[:magnetURI] =~ @sintel_trailer[:magnetURI]
      end)
    end
  end
end
