defmodule Ambroisie.MixProject do
  use Mix.Project

  defp description do
    """
    An Elixir library wrapping a webtorrent-hybrid process as an OTP application.
    """
  end

  defp package do
    {priv, 0} = System.cmd("git", ["ls-files", "priv*"])

    [
      files: Enum.drop(String.split(priv, "\n"), -1) ++ ["lib", "mix.exs", "README*", "LICENSE"],
      maintainers: ["Rigel Kent"],
      licenses: ["GPL-3.0-or-later"],
      links: %{"GitHub" => "https://github.com/rigelk/ambroisie"}
    ]
  end

  def project do
    [
      app: :ambroisie,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      aliases: aliases(),
      default_task: "all",
      preferred_cli_env: [
        all: :test
      ],
      compilers: Mix.compilers() ++ [:typescript],

      # Docs
      name: "Ambroisie",
      source_url: "https://framagit.org/rigelk/ambroisie",
      docs: [
        # The main page in the docs
        main: "Ambroisie",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {AmbroisieApp, %{restore: false}}
    ]
  end

  defp aliases do
    [
      test: "test --no-start",
      all: ["deps.get --only #{Mix.env()}", "test"]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gen_stage, "~> 0.14"},
      {:stash, "~> 1.0.0"},
      {:ex_doc, "~> 0.19", only: :dev}
    ]
  end
end
